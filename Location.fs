namespace Splorr

type Location =
    {
        x: int
        y: int
    }
    
    member x.changeBy (deltaX:int, deltaY:int) : Location =
        {
            x = x.x + deltaX
            y = x.y + deltaY
        }

module Location =

    let create (x:int, y:int) : Location =
        {
            x = x
            y = y
        }

    let changeBy (delta:int * int) (location:Location) : Location =
        location.changeBy delta