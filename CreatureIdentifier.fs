namespace Splorr

[<RequireQualifiedAccess>]
type CreatureIdentifier =
    | Rat
    | Bat
