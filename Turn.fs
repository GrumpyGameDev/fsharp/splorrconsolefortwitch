namespace Splorr

[<RequireQualifiedAccess>]
type Turn =
    | Left
    | Right
    | Around

