namespace Splorr

module MoveHandler =

    open Utility

    let private handleAttackOfOpportunity (random:System.Random) (game:Game) : Game =
        game.avatarCell.Value.creatureInstance
        |> Option.fold
            (fun g instance -> 
                g.addMessage (sprintf "Attack of opportunity from %s!" game.world.creatures.[instance.creatureIdentifier].name)
                //TODO: make actual attack of opportunity
                ) game

    let internal handleMove (random:System.Random) (input: string list) (game:Game) : Game =
        game
        |> handleAttackOfOpportunity random
        |>
            if input.IsEmpty then
                Game.addMessage "You move ahead"
                >> Game.moveAvatarInDirection game.avatar.facing
            else
                match input.Head.ToLower() with
                | "f" | "forward" | "forwards" | "ahead" ->
                    Game.addMessage "You move ahead"
                    >> Game.moveAvatarInDirection game.avatar.facing
                | "b" | "back" | "backward" | "backwards" ->
                    Game.addMessage "You move back"
                    >> Game.moveAvatarInDirection (game.avatar.facing |> Direction.opposite)
                | "l" | "left" ->
                    Game.addMessage "You move left"
                    >> Game.moveAvatarInDirection (game.avatar.facing |> Direction.previous)
                | "r" | "right" ->
                    Game.addMessage "You move right"
                    >> Game.moveAvatarInDirection (game.avatar.facing |> Direction.next)
                | "n" | "north" ->
                    Game.addMessage "You move north"
                    >> Game.moveAvatarInDirection Direction.North
                | "e" | "east" ->
                    Game.addMessage "You move east"
                    >> Game.moveAvatarInDirection Direction.East
                | "s" | "south" ->
                    Game.addMessage "You move south"
                    >> Game.moveAvatarInDirection Direction.South
                | "w" | "west" ->
                    Game.addMessage "You move west"
                    >> Game.moveAvatarInDirection Direction.West
                | _ ->
                    Game.addMessage "Move where?"
        |> StatusHandler.handleStatus random []

