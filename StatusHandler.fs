namespace Splorr

module StatusHandler =

    open Utility

    let private showBaseStatus (random:System.Random) (game:Game) : Game =
        game
        |> Game.autoCreateAvatarCell random
        |> Game.addMessage ("Status:")
        |> Game.addMessage (sprintf "You are facing %s" (game.avatar.facing |> Direction.name))
        |> Game.addMessage (sprintf "Position: (%d,%d)" game.avatar.location.x game.avatar.location.y)
        |> Game.addMessage (sprintf "Health (%d/%d)" (game.avatar.health - game.avatar.wounds) game.avatar.health)

    let private showCreatureStatus (game:Game) : Game =
        game.avatarCell.Value.creatureInstance
        |> Option.fold
            (fun g instance -> 
                let creature = game.world.creatures.[instance.creatureIdentifier]
                g.addMessage (sprintf "Creature: %s (%d/%d)" creature.name (creature.health-instance.wounds) (creature.health))
                ) game

    let private showWorldCellStatus (game:Game) : Game =
        game
        |> Game.addMessage (sprintf "Terrain: %s" (game.avatarCell.Value.terrain |> Terrain.name))
        |> showCreatureStatus

    let internal handleStatus (random:System.Random) (input: string list) (game:Game) : Game =
        game
        |> showBaseStatus random
        |> showWorldCellStatus
