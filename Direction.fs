namespace Splorr

[<RequireQualifiedAccess>]
type Direction =
    | North
    | East
    | South
    | West

module Direction =
    let name (direction:Direction) : string =
        match direction with
        | Direction.North -> "north"
        | Direction.East -> "east"
        | Direction.South -> "south"
        | Direction.West -> "west"

    //next is clockwise
    let next (direction:Direction) : Direction =
        match direction with
        | Direction.North -> Direction.East
        | Direction.East -> Direction.South
        | Direction.South -> Direction.West
        | Direction.West -> Direction.North

    //previous is counter-clockwise
    let previous (direction:Direction) : Direction =
        match direction with
        | Direction.North -> Direction.West
        | Direction.East -> Direction.North
        | Direction.South -> Direction.East
        | Direction.West -> Direction.South

    let opposite (direction:Direction) : Direction =
        match direction with
        | Direction.North -> Direction.South
        | Direction.East -> Direction.West
        | Direction.South -> Direction.North
        | Direction.West -> Direction.East

    let turn (turn:Turn) (direction:Direction) : Direction =
        match turn with
        | Turn.Left ->
            direction
            |> previous
        | Turn.Right ->
            direction
            |> next
        | Turn.Around ->
            direction
            |> opposite

    let nextLocation (location:Location) (direction:Direction) : Location =
        match direction with
        | Direction.North ->
            location
            |> Location.changeBy (0,1)
        | Direction.East ->
            location
            |> Location.changeBy (1,0)
        | Direction.South ->
            location
            |> Location.changeBy (0,-1)
        | Direction.West ->
            location
            |> Location.changeBy (-1,0)
