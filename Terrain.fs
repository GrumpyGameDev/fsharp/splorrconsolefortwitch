namespace Splorr

[<RequireQualifiedAccess>]
type Terrain =
    | Grass
    | Woods

module Terrain =
    let name (terrain:Terrain) : string =
        match terrain with
        | Terrain.Grass ->
            "grass"
        | Terrain.Woods ->
            "woods"

    let private generator =
        [
            (Terrain.Grass,10)
            (Terrain.Woods,5)
        ]
        |> Map.ofList

    let generate (random:System.Random) : Terrain =
        generator
        |> Utility.generate  random

    let private woodsCreatureGenerator: Map<CreatureIdentifier option, int> =
        [
            (None, 1)
            (Some CreatureIdentifier.Bat, 1)
        ]
        |> Map.ofList

    let private grassCreatureGenerator: Map<CreatureIdentifier option, int> =
        [
            (None, 1)
            (Some CreatureIdentifier.Rat, 1)
        ]
        |> Map.ofList

    let private creatureGenerators =
        [
            (Terrain.Grass,grassCreatureGenerator)
            (Terrain.Woods,woodsCreatureGenerator)
        ]
        |> Map.ofList

    let generateCreature (random:System.Random) (terrain:Terrain) : CreatureIdentifier option =
        let generator = creatureGenerators.[terrain]
        Utility.generate random generator
