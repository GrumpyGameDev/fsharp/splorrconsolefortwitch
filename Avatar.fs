namespace Splorr

type Avatar =
    {
        facing: Direction
        location: Location
        health: int
        wounds: int
        attack: Map<int,int>
        defend: Map<int,int>

    }
    member x.setFacing (facing:Direction) : Avatar =
        {x with facing = facing}

    member x.turn (turn:Turn) : Avatar =
        (x.facing |> Direction.turn turn)
        |> x.setFacing

    member x.moveInDirection (direction:Direction) : Avatar =
        {x with location = direction |> Direction.nextLocation x.location}

module Avatar =
    //TODO: get rid of default attack and defend maps
    let baseAttack : Map<int,int>=
        [
            (0,1)
            (1,2)
            (2,1)
        ]
        |> Map.ofList
    let baseDefend : Map<int,int>=
        [
            (0,4)
            (1,4)
            (2,1)
        ]
        |> Map.ofList

    let create () : Avatar =
        {
            facing = Direction.North
            location = Location.create (0,0)
            health = 5
            wounds = 0
            attack = baseAttack
            defend = baseDefend
        }

    let setFacing (facing:Direction) (avatar:Avatar) : Avatar =
        facing
        |> avatar.setFacing

    let turn (turn:Turn) (avatar:Avatar) : Avatar =
        turn
        |> avatar.turn

    let moveInDirection (direction:Direction) (avatar:Avatar) : Avatar =
        direction
        |> avatar.moveInDirection
