namespace Splorr

module internal Utility =

    let internal readLine = System.Console.ReadLine

    let internal generate (random:System.Random) (generator:Map<'T,int>) : 'T =
        ((None, random.Next(generator |> Map.fold (fun a _ v -> a+v) 0)), generator)
        ||> Map.fold 
            (fun (r:'T option,g:int) k v -> 
                match r with
                | Some _ ->
                    (r,0)
                | None ->
                    if g<v then
                        (Some k, 0)
                    else
                        (None, g-v)) 
        |> fst
        |> Option.get
