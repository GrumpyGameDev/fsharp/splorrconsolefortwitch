namespace Splorr

type CreatureInstance =
    {
        creatureIdentifier : CreatureIdentifier
        wounds: int
    }

module CreatureInstance =
    let create (creatureIdentifier:CreatureIdentifier) : CreatureInstance =
        {
            creatureIdentifier = creatureIdentifier
            wounds = 0
        }