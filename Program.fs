﻿open Splorr

[<EntryPoint>]
let main argv =
    Game.create()
    |> GameRunner.run (new System.Random())
    0
