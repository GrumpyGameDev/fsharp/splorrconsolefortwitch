namespace Splorr

module TurnHandler =

    open Utility

    let private turnToDirection (direction:Direction) (game:Game) : Game =
        game 
        |> Game.setAvatarFacing direction
        |> Game.addMessage (sprintf "You turn %s" (direction |> Direction.name))

    let private turnLeft (game:Game) : Game =
        game
        |> Game.turnAvatar Turn.Left
        |> Game.addMessage "You turn to the left"

    let private turnRight (game:Game) : Game =
        game
        |> Game.turnAvatar Turn.Right
        |> Game.addMessage "You turn to the left"

    let private turnAround (game:Game) : Game =
        game
        |> Game.turnAvatar Turn.Around
        |> Game.addMessage "You turn around"

    let internal handleTurn (input: string list) (game:Game) : Game =
        if input.IsEmpty then
            "Turn where?"
            |> game.addMessage
        else
            match input.Head.ToLower() with
            | "l" | "left" ->
                game
                |> turnLeft
            | "r" | "right" ->
                game
                |> turnRight
            | "a" | "around" ->
                game
                |> turnAround
            | "n" | "north" ->
                game
                |> turnToDirection Direction.North
            | "e" | "east" ->
                game
                |> turnToDirection Direction.East
            | "s" | "south" ->
                game
                |> turnToDirection Direction.South
            | "w" | "west" ->
                game
                |> turnToDirection Direction.West
            | _ -> game

