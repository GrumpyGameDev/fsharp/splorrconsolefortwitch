namespace Splorr

module GameRunner =

    open Utility

    let private processInput (random:System.Random) (input:string list) (game:Game) : Game =
        match input.Head.ToLower() with
        | "help" ->
            game
            |> HelpHandler.handleHelp input.Tail
        | "attack" ->
            game
            |> AttackHandler.handleAttack random input.Tail
        | "quit" -> 
            game 
            |> QuitHandler.confirmQuit
        | "status" ->
            game
            |> StatusHandler.handleStatus random input.Tail
        | "turn" ->
            game
            |> TurnHandler.handleTurn input.Tail
        | "move" ->
            game
            |> MoveHandler.handleMove random input.Tail
        | "n" | "e" | "s" | "w" | "north" | "east" | "south" | "west" ->
            game
            |> MoveHandler.handleMove random input
        | _ -> 
            game.addMessage "Huh?"

    let private showStatus (game:Game) : unit =
        game.messages
        |> List.iter (printfn "%s")

    let private doGameLoop (random:System.Random) (game:Game) : Game =
        game
        |> showStatus

        printf ">"

        (readLine().Split(' ') |> Array.toList, game.clearMessages())
        ||> processInput random

    let rec run (random:System.Random) (game:Game) : unit =
        game.state
        |> Option.iter
            (fun _ ->
                let game' = doGameLoop random game
                run random game')
