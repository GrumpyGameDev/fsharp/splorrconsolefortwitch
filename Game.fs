namespace Splorr

type Game = 
    {
        state:GameState option
        avatar: Avatar
        messages: string list
        world: World
    }
    member x.setGameState (state:GameState) : Game =
        {x with state = state |> Some}

    member x.clearGameState () : Game =
        {x with state = None}

    member x.transformAvatar (transform: Avatar -> Avatar) : Game =
        {x with avatar = x.avatar |> transform}

    member x.transformWorld (transform: World -> World) : Game =
        {x with world = x.world |> transform}

    member x.clearMessages () : Game =
        {x with messages = []}

    member x.addMessage (message:string) : Game =
        {x with messages = List.append x.messages [message]}

    member x.moveAvatarInDirection (direction:Direction) : Game =
        x.transformAvatar (Avatar.moveInDirection direction)

    member x.autoCreateAvatarCell (random:System.Random) : Game =
        x.transformWorld (World.autoCreateCell random x.avatar.location)

    member x.avatarCell : WorldCell option =
        x.world.cells.TryFind x.avatar.location


module Game =
    let setGameState (state:GameState) (game:Game): Game =
        state
        |> game.setGameState

    let clearGameState  (game:Game) : Game =
        game.clearGameState()

    let create () : Game =
        {
            state = GameState.InPlay |> Some
            avatar = Avatar.create()
            messages = []
            world = World.create()
        }

    let setAvatarFacing (direction: Direction) (game:Game) : Game =
        game.transformAvatar(Avatar.setFacing direction)

    let addMessage (message:string) (game:Game) : Game =
        game.addMessage message

    let turnAvatar (turn:Turn) (game:Game) : Game =
        game.transformAvatar(Avatar.turn turn)

    let moveAvatarInDirection (direction:Direction) (game:Game) : Game =
        game.moveAvatarInDirection (direction)

    let autoCreateAvatarCell (random:System.Random) (game:Game) : Game =
        game.autoCreateAvatarCell random

    let damageAvatarCellCreatureInstance (damage:int) (game:Game) : Game =
        //TODO: implement me
        game

    let damageAvatar (damage:int) (game:Game) : Game =
        //TODO: implement me
        game

