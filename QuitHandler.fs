namespace Splorr

module QuitHandler =

    open Utility

    let internal confirmQuit (game:Game) : Game =
        printf "To quit, type 'quit' again.\n>"
        match readLine().ToLower() with
        | "quit" -> game |> Game.clearGameState
        | _ -> game.addMessage "Quit canceled"

