namespace Splorr

module HelpHandler =

    open Utility

    let private showCommandHelp (game:Game) : Game =
        game
        |> Game.addMessage "Command Help:"
        |> Game.addMessage "\thelp - this screen"
        |> Game.addMessage "\tstatus - show current status"
        |> Game.addMessage "\tquit - quit the game"
        |> Game.addMessage "\tattack - attack creature"
        |> Game.addMessage "\tturn - turn the avatar"
        |> Game.addMessage "\tmove - move the avatar"
        |> Game.addMessage "\tn | e | s | w - move the avatar"
        |> Game.addMessage "\tnorth | east | south | west - move the avatar"

    let private showMoveHelp (game:Game) : Game =
        game
        |> Game.addMessage "move <direction>"
        |> Game.addMessage "\tDirections:"
        |> Game.addMessage "\tn | north - absolute direction"
        |> Game.addMessage "\te | east - absolute direction"
        |> Game.addMessage "\ts | south - absolute direction"
        |> Game.addMessage "\tw | west - absolute direction"
        |> Game.addMessage "\tl | left - move left according to current facing"
        |> Game.addMessage "\tr | right - move right according to current facing"
        |> Game.addMessage "\tf | forward | forwards | ahead - move ahead according to current facing"
        |> Game.addMessage "\tb | back | backward | backwards - move back according to current facing"

    let private showTurnHelp (game:Game) : Game =
        game
        |> Game.addMessage "turn <facing>"
        |> Game.addMessage "\tFacing:"
        |> Game.addMessage "\tn | north - absolute direction"
        |> Game.addMessage "\te | east - absolute direction"
        |> Game.addMessage "\ts | south - absolute direction"
        |> Game.addMessage "\tw | west - absolute direction"
        |> Game.addMessage "\tl | left - turn left according to current facing"
        |> Game.addMessage "\tr | right - turn right according to current facing"
        |> Game.addMessage "\ta | around - turn around according to current facing"

    let internal handleHelp (input: string list) (game:Game) : Game =
        if input.IsEmpty then
            game
            |> showCommandHelp
        else
            match input.Head.ToLower() with
            | "turn" ->
                game
                |> showTurnHelp
            | "move" ->
                game
                |> showMoveHelp
            | _ ->
                game
                |> showCommandHelp
    
