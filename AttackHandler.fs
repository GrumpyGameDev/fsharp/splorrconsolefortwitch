namespace Splorr

module AttackHandler =

    open Utility

    let private rollAttack (random:System.Random) (attack:Map<int,int>, defend:Map<int,int>) : int * int * (int option) =
        let attackRoll = 
            attack
            |> Utility.generate random
        let defendRoll =
            defend
            |> Utility.generate random
        let damage =
            match attackRoll - defendRoll with
            | x when x>0 -> x |> Some
            | _ -> None
        (attackRoll, defendRoll, damage)

    let private handleAvatarAttack (random:System.Random) (game:Game) : Game =
        let instance = 
            game.avatarCell.Value.creatureInstance    
            |> Option.get

        let avatarAttackRoll, instanceDefendRoll, damage = 
            rollAttack random (game.avatar.attack, game.world.creatures.[instance.creatureIdentifier].defend)

        match damage with
        | Some x ->
            game
            |> Game.addMessage (sprintf "You hit for %d!" x)
            |> Game.damageAvatarCellCreatureInstance x
        | _ ->
            game
            |> Game.addMessage "You miss!"

    let private handleInstanceCounterAttack  (random:System.Random) (game:Game) : Game =
        let instance = 
            game.avatarCell.Value.creatureInstance    
            |> Option.get

        let creature = game.world.creatures.[instance.creatureIdentifier]

        let instanceAttackRoll, avatarDefendRoll, damage = 
            rollAttack random (creature.attack, game.avatar.defend)

        match damage with
        | Some x ->
            game
            |> Game.addMessage (sprintf "%s hits for %d!" creature.name x)
            |> Game.damageAvatar x
        | _ ->
            game
            |> Game.addMessage (sprintf "%s misses!" creature.name)

    let private handleInstanceDeath (game:Game) : Game =
        let instance = 
            game.avatarCell.Value.creatureInstance    
            |> Option.get
        //TODO: check for and resolve instance death
        game

    let internal handleAttack (random:System.Random) (input: string list) (game:Game) : Game =
        match game.avatarCell.Value.creatureInstance with
        | Some _ ->
            game
            |> handleAvatarAttack random
            |> handleInstanceCounterAttack random
            |> handleInstanceDeath
        | _ ->
            game
            |> Game.addMessage "There is nothing to attack!"
    
