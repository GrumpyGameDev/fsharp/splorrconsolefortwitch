namespace Splorr

type World =
    {
        cells: Map<Location, WorldCell>
        creatures: Map<CreatureIdentifier, Creature>
    }

    member x.setCell (location:Location) (cell:WorldCell) : World =
        {x with cells = x.cells |> Map.add location cell}

    member x.autoCreateCell (random:System.Random) (location:Location) : World =
        match x.cells |> Map.tryFind location with
        | None ->
            WorldCell.create(random)
            |> x.setCell location
        | _ ->
            x

module World =
    let private attack1 =
        [
            (0,2)
            (1,1)
        ]
        |> Map.ofList
    let private defend1 =
        [
            (0,5)
            (1,1)
        ]
        |> Map.ofList
    let private defend2 =
        [
            (0,25)
            (1,10)
            (2,1)
        ]
        |> Map.ofList
    let private batCreature =
        {
            name="bat"
            attack = attack1
            defend = defend2
            health = 1
        }
    let private ratCreature =
        {
            name="rat"
            attack = attack1
            defend = defend1
            health = 1
        }
    let private creatures =
        [
            (CreatureIdentifier.Bat, batCreature)
            (CreatureIdentifier.Rat, ratCreature)
        ]
        |> Map.ofList

    let create () : World =
        {
            cells = Map.empty
            creatures = creatures
        }

    let autoCreateCell (random:System.Random) (location:Location) (world:World) : World =
        world.autoCreateCell random location
