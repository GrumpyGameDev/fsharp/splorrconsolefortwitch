namespace Splorr

type Creature =
    {
        name   : string
        attack : Map<int,int>
        defend : Map<int,int>
        health : int
    }
