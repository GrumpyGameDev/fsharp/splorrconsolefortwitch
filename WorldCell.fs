namespace Splorr

type WorldCell =
    {
        terrain: Terrain
        creatureInstance: CreatureInstance option
    }

module WorldCell =
    let create (random:System.Random) : WorldCell =
        let terrain = 
            Terrain.generate random
        let creatureInstance = 
            Terrain.generateCreature random terrain
            |> Option.map CreatureInstance.create
        {
            //TODO: visited or visit count
            terrain = terrain
            creatureInstance = creatureInstance
        }
